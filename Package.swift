// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "NetMind",
    platforms: [
        .macOS(.v10_12), .iOS(.v10), .watchOS(.v3), .tvOS(.v10)
    ],
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "NetMind",
            targets: ["NetMind"]),
    ],
    dependencies: [
        .package(url: "https://github.com/steadysense/core-mind", from: "1.0.3")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "NetMind",
            dependencies: ["CoreMind"]),
        .testTarget(
            name: "NetMindTests",
            dependencies: ["NetMind", "CoreMind"]),
    ]
)
