//
//  WebserviceRequest.swift
//  topmindKit
//
//  Created by Martin Gratzer on 18/03/2017.
//  Copyright © 2017 topmind development. All rights reserved.
//

import Foundation
import CoreMind

public enum HttpMethod: String {
    case post = "POST", get = "GET", delete = "DELETE", put = "PUT", patch = "PATCH"
}

public protocol WebserviceRequest {
    /// HTTP query parameters for the request
    var queryParameters: [String: String] { get }
    /// Additional sub path for the request
    var path: String { get }
    /// HTTP Method for the request
    var method: HttpMethod { get }
    /// Data encoding for the given request payload.
    func encode(with format: WebserviceFormat) -> Result<Data>?
    /// Data decoding for the request's response, use this method to define custom date formatting.
    /// Default Implementation uses caller's formatter default.
    func decode<T: Decodable>(response: WebserviceResponse, with format: WebserviceFormat) -> Result<T>
}

extension WebserviceRequest {
    /// Default implementation, override if customizations are required (like date formatting)
    public func decode<T: Decodable>(response: WebserviceResponse, with format: WebserviceFormat) -> Result<T> {
        return format.deserialize(decodable: response.data)
    }
}

extension WebserviceRequest {
    func url(for serviceUrl: URL) -> Result<URL> {
        let url = serviceUrl.appendingPathComponent(path)
        guard var components = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
            return .failure("Could not create URL for \(serviceUrl)")
        }

        components.queryItems = queryParameters.map {
            URLQueryItem(name: $0.key, value: "\($0.value)")
        }

        guard let requestUrl = components.url else {
            return .failure("Could not create URL for \(serviceUrl)")
        }

        return .success(requestUrl)
    }
}
