//
//  WebserviceFormat.swift
//  topmindKit
//
//  Created by Martin Gratzer on 18/03/2017.
//  Copyright © 2017 topmind development. All rights reserved.
//

import Foundation
import CoreMind

public protocol WebserviceFormat {
    static var httpHeaders: [String: String] { get }

    @available(*, deprecated, message: "Use `serialize<T: Encodable>(encodable: T) -> Result<Data>`` instead")
    func encode(payload: Any) -> Result<Data>

    func serialize<T: Encodable>(encodable: T) -> Result<Data>

    @available(iOS 10.0, *)
    func serialize<T: Encodable>(encodable: T, dateEncodingStrategy: JSONEncoder.DateEncodingStrategy) -> Result<Data>

    func deserialize<T: Decodable>(decodable: Data) -> Result<T>

    @available(iOS 10.0, *)
    func deserialize<T: Decodable>(decodable: Data, dateDecodingStrategy: JSONDecoder.DateDecodingStrategy) -> Result<T>
}
